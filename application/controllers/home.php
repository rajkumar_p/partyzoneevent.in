<?php 

defined('BASEPATH') OR exit('No direct script access allowed');


class Home extends CI_Controller{
    
    public function index(){
        $this->load->view('navbar/nav');
        $this->load->view('home_page');
        $this->load->view('footer/footer');
    }

    public function contact(){
        $this->load->view('navbar/nav');
        $this->load->view('contact');
        $this->load->view('footer/footer');
    }

    public function service(){
        $this->load->view('navbar/nav');
        $this->load->view('service');
        $this->load->view('footer/footer');
    }

    public function about(){
        $this->load->view('navbar/nav');
        $this->load->view('about_us');
        $this->load->view('footer/footer');
    }
}

?>